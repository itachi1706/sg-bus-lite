import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:sg_bus_lite/models/busstops.dart';
import 'package:sg_bus_lite/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SavedBuses {
  static SavedBuses? _instance;

  static const String _key = 'saved_buses';

  Map<String, BusStops> _savedStops = <String, BusStops>{};

  SavedBuses() {
    _init();
  }

  void toggleFavStatus(BusStops stop, BuildContext context) {
    if (isStopFavourited(stop.code)) {
      // Remove
      _savedStops.remove(stop.code);
      Util.showSnackbarQuick(context,
          'Removed ${stop.description} (${stop.code}) from favourites');
    } else {
      // Add
      _savedStops[stop.code] = stop;
      Util.showSnackbarQuick(
          context, 'Added ${stop.description} (${stop.code}) to favourites');
    }
    _updatePrefString();
  }

  bool isStopFavourited(String stopCode) {
    return _savedStops.containsKey(stopCode);
  }

  List<BusStops> getAllFavourites() {
    return _savedStops.values.toList();
  }

  static SavedBuses getInstance() {
    _instance ??= SavedBuses();

    return _instance!;
  }

  void _init() async {
    var pref = await _getPrefManager();
    var busJson = pref.getString(_key);
    busJson ??= '';
    print('[SAVED-DBG] Bus JSON: $busJson');
    var busData = <String, dynamic>{};
    if (busJson != '') {
      busData = jsonDecode(busJson);
    }
    print('[SAVED-DBG] Decoded: $busData');

    // process bus data to stop list
    var stops = <String, BusStops>{};
    busData.forEach((key, value) {
      var bsd = BusStops.fromJson(value);
      stops[bsd.code] = bsd;
    });
    _savedStops = stops;
    print('[SAVED] Found ${_savedStops.length} saved bus stops');
  }

  // SharedPreference
  Future<SharedPreferences> _getPrefManager() async {
    return await SharedPreferences.getInstance();
  }

  void _updatePrefString() async {
    var pref = await _getPrefManager();
    var stops = jsonEncode(_savedStops);
    print('[SAVED-DBG] Encoded: $stops');
    await pref.setString(_key, stops);
  }
}
