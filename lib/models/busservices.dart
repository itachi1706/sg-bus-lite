class BusServices {
  String svcNo = '';

  String operator = '';

  BusServicesEstimate? bus1;

  BusServicesEstimate? bus2;

  BusServicesEstimate? bus3;

  BusServices({
    required this.svcNo,
    required this.operator,
    this.bus1,
    this.bus2,
    this.bus3,
  });

  factory BusServices.fromJson(Map<String, dynamic> json, DateTime? curTime) {
    curTime ??= DateTime.now();

    return BusServices(
      svcNo: json['ServiceNo'],
      operator: json['Operator'],
      bus1: (json['NextBus'] != '' && json['NextBus']['EstimatedArrival'] != '') ? BusServicesEstimate.fromJson(json['NextBus'], curTime) : null,
      bus2: (json['NextBus2'] != '' && json['NextBus2']['EstimatedArrival'] != '') ? BusServicesEstimate.fromJson(json['NextBus2'], curTime) : null,
      bus3: (json['NextBus3'] != '' && json['NextBus3']['EstimatedArrival'] != '') ? BusServicesEstimate.fromJson(json['NextBus3'], curTime) : null,
    );
  }

  static List<BusServices> fromDataset(Map<String, dynamic> json, {DateTime? curTime}) {
    List<dynamic> jData = json['Services'];

    var svcList = <BusServices>[];
    jData.forEach((value) {
      var bsd = BusServices.fromJson(value, curTime);
      svcList.add(bsd);
    });

    return svcList;
  }
}

class BusServicesEstimate {
  String originCode = '';
  String destinationCode = '';
  String estimatedArrivalString = '';
  String latitude = '';
  String longitude = '';
  String visit = '';
  String load = '';
  String feature = '';
  String type = '';

  DateTime? arrivalTime;
  bool wheelchairSupport = false;
  int loadLevel = 1;
  String busType;
  int minsLeft = -1;

  BusServicesEstimate({
    required this.originCode,
    required this.destinationCode,
    required this.estimatedArrivalString,
    required this.latitude,
    required this.longitude,
    required this.visit,
    required this.load,
    required this.feature,
    required this.type,
    required this.arrivalTime,
    required this.wheelchairSupport,
    required this.loadLevel,
    required this.busType,
    required this.minsLeft,
  });

  factory BusServicesEstimate.fromJson(Map<String, dynamic> json, DateTime curDate) {

    var date = DateTime.parse(json['EstimatedArrival']);
    var minsLeft = date.difference(curDate).inMinutes;

    return BusServicesEstimate(
      originCode: json['OriginCode'],
      destinationCode: json['DestinationCode'],
      estimatedArrivalString: json['EstimatedArrival'],
      latitude: json['Latitude'],
      longitude: json['Longitude'],
      visit: json['VisitNumber'],
      load: json['Load'],
      feature: json['Feature'],
      type: json['Type'],
      arrivalTime: date,
      wheelchairSupport: (json['Feature'] == 'WAB'),
      loadLevel: getLoadLevel(json['Load']),
      busType: getBusType(json['Type']),
      minsLeft: minsLeft,
    );
  }

  static int getLoadLevel(String load) {
    switch (load) {
      case 'SEA': return 1; // Seats Available (Green)
      case 'SDA': return 2; // Standing Available (Yellow)
      default: return 3; // Limited Standing (Red)
    }
  }

  static String getBusType(String bt) {
    var typeStr = 'Unknown';
    switch (bt) {
      case 'SD': typeStr = 'Single'; break;
      case 'DD': typeStr = 'Double'; break;
      case 'DB': typeStr = 'Bendy'; break;
    }

    return typeStr;
  }
}