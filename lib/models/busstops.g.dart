// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'busstops.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BusStopsAdapter extends TypeAdapter<BusStops> {
  @override
  final int typeId = 1;

  @override
  BusStops read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BusStops(
      code: fields[0] as String,
      road: fields[1] as String,
      description: fields[2] as String,
      latitude: fields[3] as double,
      longitude: fields[4] as double,
    );
  }

  @override
  void write(BinaryWriter writer, BusStops obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.code)
      ..writeByte(1)
      ..write(obj.road)
      ..writeByte(2)
      ..write(obj.description)
      ..writeByte(3)
      ..write(obj.latitude)
      ..writeByte(4)
      ..write(obj.longitude);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BusStopsAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
