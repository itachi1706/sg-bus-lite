import 'package:hive_flutter/hive_flutter.dart';

part 'busstops.g.dart';

// Run 'flutter packages pub run build_runner build' to generate

@HiveType(typeId: 1)
class BusStops {
  @HiveField(0)
  String code = '';

  @HiveField(1)
  String road = '';

  @HiveField(2)
  String description = '';

  @HiveField(3)
  double latitude;

  @HiveField(4)
  double longitude;

  BusStops({
    required this.code,
    required this.road,
    required this.description,
    required this.latitude,
    required this.longitude,
  });

  factory BusStops.fromJson(Map<String, dynamic> json) {
    return BusStops(
      latitude: json['Latitude'],
      code: json['BusStopCode'],
      road: json['RoadName'],
      description: json['Description'],
      longitude: json['Longitude'],
    );
  }

  static Map<String, BusStops> fromDataset(Map<String, dynamic> json) {
    List<dynamic> jData = json['value'];

    var stops = <String, BusStops>{};
    jData.forEach((value) {
      var bsd = BusStops.fromJson(value);
      stops[bsd.code] = bsd;
    });

    return stops;
  }

  Map<String, dynamic> toJson() => {
        'Latitude': latitude,
        'BusStopCode': code,
        'Longitude': longitude,
        'RoadName': road,
        'Description': description,
      };
}
