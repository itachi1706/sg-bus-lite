import 'package:flutter/material.dart';
import 'package:sg_bus_lite/util.dart';

class PlaceholderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Placeholder Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Page Coming Soon'),
          ],
        ),
      ),
    );
  }
}

class PlaceholderWidgetContainer extends StatelessWidget {
  final Color color;

  PlaceholderWidgetContainer(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Coming Soon!',
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
          ],
        ),
      ),
    );
  }
}

class PlaceholderUtil {
  static void showUnimplementedSnackbar(BuildContext context) {
    Util.showSnackbarQuick(context, 'Feature Coming Soon!');
  }
}