import 'dart:io';

import 'package:about/about.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:sg_bus_lite/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String _version = 'Loading';
  String _versionStr = 'Unknown';
  bool _darkMode = false;

  SharedPreferences? _prefs;

  @override
  void initState() {
    super.initState();
    _refresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Settings')),
      body: SettingsList(
        contentPadding: const EdgeInsets.only(top: 16),
        sections: [
          _appDataSettings(),
          SettingsSection(
            tiles: [
              SettingsTile(
                title: 'About This App',
                leading: Icon(Icons.info_outline),
                trailing: SizedBox.shrink(),
                onPressed: _showAboutPage,
              ),
            ],
          ),
          CustomSection(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 22, bottom: 8),
                  child: Text(
                    _version,
                    style: TextStyle(color: Color(0xFF777777)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _refresh() async {
    var pref = await SharedPreferences.getInstance();

    var pkgInfo = await PackageInfo.fromPlatform();
    var version = pkgInfo.version;
    var build = pkgInfo.buildNumber;
    var type = (kIsWeb)
        ? 'Web'
        : (Platform.isAndroid)
            ? 'Android'
            : (Platform.isIOS)
                ? 'iOS'
                : 'Others';

    setState(() {
      _prefs = pref;
      _darkMode = _prefs?.getBool('dark_mode') ?? false;
      _version = 'Version: $version build $build ($type)';
      _versionStr = version;
    });
  }

  SettingsSection _appDataSettings() {
    return SettingsSection(
      tiles: [
        SettingsTile.switchTile(
          title: 'Dark Mode',
          leading: Icon(Icons.wb_sunny_outlined),
          onToggle: (bool value) {
            _prefs?.setBool('dark_mode', value).then((value) {
              Util.themeNotifier.toggleTheme();
              Util.showSnackbarQuick(
                  context, 'Relaunch app if you run into issues');
            });
            setState(() {
              _darkMode = value;
            });
          },
          switchValue: _darkMode,
        ),
      ],
    );
  }

  void _showAboutPage(BuildContext context) {
    showAboutPage(
      context: context,
      title: Text('About this app'),
      values: {
        'version': _versionStr,
        'year': DateTime.now().year.toString(),
      },
      applicationLegalese: 'Copyright © Kenneth Soh, {{ year }}',
      applicationDescription: const Text(
        'Simple Bus Tracking Application written in Flutter',
      ),
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.source_outlined),
          trailing: SizedBox.shrink(),
          title: Text('View Source Code'),
          onTap: () => Util.launchWebPage(
            'https://gitlab.com/itachi1706/sg-bus-lite',
          ),
        ),
        ListTile(
          leading: Icon(Icons.bug_report),
          trailing: SizedBox.shrink(),
          title: Text('Report a Bug/Feature Suggestion'),
          onTap: () => Util.launchWebPage(
            'https://gitlab.com/itachi1706/sg-bus-lite/-/issues/new',
          ),
        ),
        MarkdownPageListTile(
          icon: Icon(Icons.list),
          title: const Text('Changelog'),
          filename: 'CHANGELOG.md',
        ),
        LicensesPageListTile(
          title: Text('Open Source Licenses'),
          icon: Icon(Icons.favorite),
        ),
      ],
      applicationIcon: SizedBox(
        width: 100,
        height: 100,
        child: Image.asset('assets/splash/logo.png'),
      ),
    );
  }
}
