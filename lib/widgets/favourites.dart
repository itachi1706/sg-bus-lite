import 'package:flutter/material.dart';
import 'package:sg_bus_lite/helpers/savedbuses.dart';
import 'package:sg_bus_lite/models/busstops.dart';

class FavouritesTab extends StatefulWidget {
  final Function jumpWidgetFunc;

  FavouritesTab(this.jumpWidgetFunc);

  @override
  _FavouritesTabState createState() => _FavouritesTabState();
}

class _FavouritesTabState extends State<FavouritesTab> {
  List<BusStops> _favouriteStops = <BusStops>[];

  @override
  void initState() {
    super.initState();
    _refreshList();
  }

  @override
  Widget build(BuildContext context) {
    if (_favouriteStops.isEmpty) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('There are no favourite bus stops. Add one now!'),
            ],
          ),
        ),
      );
    }

    return ListView.separated(
      itemBuilder: (context, i) {
        var stop = _favouriteStops[i];

        return Card(
          child: ListTile(
            title: Text(stop.description),
            subtitle: Text('${stop.road} (${stop.code})'),
            trailing: IconButton(
              onPressed: () => _unfavouriteStop(stop),
              icon: Icon(Icons.delete_forever),
              color: Colors.redAccent,
            ),
            onTap: () => _selectStop(stop),
          ),
        );
      },
      separatorBuilder: (context, i) {
        return Divider(height: 0);
      },
      itemCount: _favouriteStops.length,
    );
  }

  void _selectStop(BusStops stop) {
    widget.jumpWidgetFunc(stop.code);
  }

  void _unfavouriteStop(BusStops stop) {
    SavedBuses.getInstance().toggleFavStatus(stop, context);
    _refreshList();
  }

  void _refreshList() {
    var stops = SavedBuses.getInstance().getAllFavourites();
    setState(() {
      _favouriteStops = stops;
    });
  }
}
