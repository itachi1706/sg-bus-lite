import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:sg_bus_lite/models/busstops.dart';

class SearchTab extends StatefulWidget {
  final Function jumpWidgetFunc;

  SearchTab(this.jumpWidgetFunc);

  @override
  _SearchTabState createState() => _SearchTabState();
}

class _SearchTabState extends State<SearchTab> {
  List<BusStops> _results = <BusStops>[];

  final _searchController = StreamController<Future>();
  StreamSubscription? _sub;
  Box<BusStops>? _box;

  @override
  void initState() {
    super.initState();
    _sub =
        _searchController.stream.asyncMap((event) async => await event).listen(
      (_) {
        print('Listening');
      },
      cancelOnError: false,
    );
    _init();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Column(
          children: [
            TextField(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.all(4),
                border: UnderlineInputBorder(),
                hintText: 'Search with Bus Stop Code or Name',
              ),
              onChanged: _updateText,
            ),
            _getList(),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    print('Disposing Elements');
    _sub?.cancel();
    _box?.close();
  }

  Widget _getList() {
    if (_results.isEmpty) {
      return Padding(
        padding: EdgeInsets.only(top: 8),
        child: Center(
          child: Text('No Results'),
        ),
      );
    }

    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(top: 8),
        child: ListView.separated(
          //physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            var stop = _results[i];

            return Card(
              child: ListTile(
                title: Text(stop.description),
                subtitle: Text('${stop.road} (${stop.code})'),
                onTap: () => _selectStop(stop),
              ),
            );
          },
          separatorBuilder: (context, i) {
            return Divider(height: 0);
          },
          itemCount: _results.length,
        ),
      ),
    );
  }

  void _init() async {
    _box = await Hive.openBox<BusStops>('buses');

    await _getData(null);
  }

  Future<void> _getData(String? search) async {
    if (_box == null) {
      return;
    }
    var dt = _box!.values.toList();

    if (search != null) {
      dt = dt
          .where((element) =>
              element.code.toLowerCase().contains(search.toLowerCase()) ||
              element.road.toLowerCase().contains(search.toLowerCase()) ||
              element.description.toLowerCase().contains(search.toLowerCase()))
          .toList();
    }

    setState(() {
      _results = dt;
    });
  }

  void _selectStop(BusStops stop) {
    widget.jumpWidgetFunc(stop.code);
  }

  void _updateText(String query) async {
    _searchController.add(_getData(query));
  }
}
