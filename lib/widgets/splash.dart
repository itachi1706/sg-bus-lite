import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:sg_bus_lite/helpers/savedbuses.dart';
import 'package:sg_bus_lite/models/busstops.dart';
import 'package:sg_bus_lite/util.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashScreenPage extends StatelessWidget {
  var _initialized = false;

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      navigateAfterFuture: _initialize(),
      image: Image.asset('assets/splash/logo.png'),
      loadingText: Text('Initializing App'),
      title: Text('SG Bus Where?',
          style: TextStyle(
              fontSize: 20.0,
              fontFamily: 'Product-Sans-Bold',
              color: Colors.white)),
      backgroundColor: Colors.black,
      photoSize: 100.0,
    );
  }

  Future<void> _updateDb() async {
    if (!_initialized) {
      await Hive.initFlutter();
      Hive.registerAdapter(BusStopsAdapter());
      _initialized = true;
    }

    var busDb = await Hive.openBox('buses');

    // Get data from internet
    var response =
        await http.get(Uri.parse('${Util.baseURL}/busstops.php?api=2'));
    print('Bus Stops HTTP code: ${response.statusCode}');
    //print('Response body: ${response.body}');

    var busData = BusStops.fromDataset(jsonDecode(response.body));
    //print(busData);

    await busDb.clear();
    await busDb.putAll(busData);

    await busDb.compact();
    await busDb.close();

    SavedBuses.getInstance(); // Init here first
  }

  Future<String> _initialize() async {
    print(await Util.getPacakgeName());
    print(await Util.getPackageSig());

    await Future.wait(
      [
        _updateDb(),
        Future.delayed(Duration(seconds: 2)),
      ],
    );

    return '/';
  }
}
