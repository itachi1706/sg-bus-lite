import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:sg_bus_lite/helpers/savedbuses.dart';
import 'package:sg_bus_lite/models/busservices.dart';
import 'package:sg_bus_lite/models/busstops.dart';
import 'package:sg_bus_lite/placeholder.dart';
import 'package:sg_bus_lite/util.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:sortedmap/sortedmap.dart';

class BusArrivalsTab extends StatefulWidget {
  final String? parentCode;

  BusArrivalsTab(this.parentCode);

  @override
  _BusArrivalsTabState createState() => _BusArrivalsTabState();
}

class _BusArrivalsTabState extends State<BusArrivalsTab> {
  final PanelController _supPc = PanelController();

  LocationPermission? _hasLocationPerm;
  Position? _currentPosition;
  SortedMap<double, BusStops>? _nearbyBusStops;
  List<BusServices> _currentServices = <BusServices>[];
  BusStops? _currentStop;

  bool _currentFavourited = false;

  @override
  void initState() {
    super.initState();
    _checkPermission();
  }

  @override
  Widget build(BuildContext context) {
    var tR = Radius.circular(24);

    if (_hasLocationPerm == null ||
        (_doIHaveLocationPermissions() &&
            (_currentPosition == null || _nearbyBusStops == null))) {
      return Util.centerLoadingCircle('');
    }

    if (_hasLocationPerm == LocationPermission.denied ||
        _hasLocationPerm == LocationPermission.deniedForever) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Location permission not granted or disabled'),
              ElevatedButton(
                onPressed: _noLocButtonClicked,
                child: Text((_hasLocationPerm == LocationPermission.denied)
                    ? 'Grant Location Permission'
                    : 'Open App Settings'),
              ),
            ],
          ),
        ),
      );
    }

    // Get current stop distance from me
    if (_currentStop == null) {
      setState(() {
        _nearbyBusStops = null;
      });
      _getNearbyStops();

      return Container();
    }

    var distance = Geolocator.distanceBetween(
      _currentPosition!.latitude,
      _currentPosition!.longitude,
      _currentStop!.latitude,
      _currentStop!.longitude,
    );
    var curStop = _currentStop!;

    _currentFavourited =
        SavedBuses.getInstance().isStopFavourited(curStop.code);

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            children: [
              IconButton(
                onPressed: () => _favouriteButtonPressed(context, curStop),
                icon: (_currentFavourited)
                    ? Icon(Icons.star)
                    : Icon(Icons.star_border),
                color: (_currentFavourited)
                    ? Colors.orange
                    : (Util.themeNotifier.isDarkMode()
                        ? Colors.white
                        : Colors.black),
              ),
              Expanded(
                child: Column(
                  children: [
                    Text(
                      '${curStop.description}',
                      style: TextStyle(
                        fontFamily: 'Product-Sans-Bold',
                        fontSize: 20,
                      ),
                    ),
                    Text('${curStop.code} ${curStop.road}'),
                    Text(
                      '${(distance).toStringAsFixed(2)} m from your location',
                    ),
                    //Text('Key Landmarks?'),
                  ],
                ),
              ),
              IconButton(
                onPressed: _locationButtonPressed,
                icon: Icon(Icons.my_location),
              ),
            ],
          ),
        ),
        Divider(),
        Expanded(
          child: RefreshIndicator(
            onRefresh: _refreshButtonPressed,
            child: SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: _generateServiceGrid(),
            ),
          ),
        ),
        SlidingUpPanel(
          minHeight: 48,
          maxHeight: 240,
          color:
              Util.themeNotifier.isDarkMode() ? Colors.black12 : Colors.white,
          margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24.0),
            topRight: tR,
          ),
          panelBuilder: (ScrollController sc) => _nearbyStops(sc),
          controller: _supPc,
        ),
      ],
    );
  }

  void _favouriteButtonPressed(BuildContext context, BusStops curStop) {
    SavedBuses.getInstance().toggleFavStatus(curStop, context);
    setState(() {
      _currentFavourited = !_currentFavourited;
    });
  }

  Widget _generateServiceGrid() {
    if (_currentServices.isEmpty) {
      if (_currentStop != null) {
        _getServicesFromStop(_currentStop!);
      }

      return Util.centerLoadingCircle('Getting services');
    }
    print('Services for stop: ${_currentServices.length}');

    var childs = <Widget>[];
    _currentServices.forEach((bs) {
      childs.add(_generateBusCards(bs));
    });

    return GridView.count(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 3,
      children: childs,
    );
  }

  Widget _generateBusCards(BusServices bs) {
    return FocusedMenuHolder(
      menuItems: [
        FocusedMenuItem(
          title: Text('Notify on Bus Arrival'),
          trailingIcon: Icon(Icons.bus_alert),
          onPressed: () => PlaceholderUtil.showUnimplementedSnackbar(context),
        ),
        FocusedMenuItem(
          title: Text('View Full Bus Service Route'),
          trailingIcon: Icon(Icons.format_list_bulleted_sharp),
          onPressed: () => PlaceholderUtil.showUnimplementedSnackbar(context),
        ),
        FocusedMenuItem(
          title: Text('See Bus Locations'),
          trailingIcon: Icon(Icons.map),
          onPressed: () => PlaceholderUtil.showUnimplementedSnackbar(context),
        ),
      ],
      menuWidth: MediaQuery.of(context).size.width * 0.6,
      blurSize: 5.0,
      menuItemExtent: 45,
      menuBoxDecoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      duration: Duration(milliseconds: 100),
      animateMenuItems: true,
      blurBackgroundColor: Colors.black54,
      openWithTap: true,
      menuOffset: 10.0,
      bottomOffsetHeight: 80.0,
      onPressed: () => {},
      child: _generateInnerBusCard(bs),
    );
  }

  Widget _generateInnerBusCard(BusServices bs) {
    return Card(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
            child: Center(
              child: Text(
                '${bs.svcNo}',
                style: TextStyle(
                  fontFamily: 'Product-Sans-Bold',
                  fontSize: 20,
                ),
              ),
            ),
          ),
          Center(
            child: Text(
              '${bs.operator}',
              style: TextStyle(fontSize: 8),
            ),
          ),
          Divider(
            height: 1,
          ),
          Expanded(
            child: IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  _generateArrival(bs.bus1),
                  //Spacer(),
                  VerticalDivider(width: 0),
                  _generateArrival(bs.bus2),
                  //Spacer(),
                  VerticalDivider(width: 0),
                  //Spacer(),
                  _generateArrival(bs.bus3),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _refreshButtonPressed() async {
    setState(() {
      _currentServices.clear();
    });
  }

  void _locationButtonPressed() {
    _checkPermission();

    setState(() {
      _currentServices.clear();
      _nearbyBusStops?.clear();
      _currentStop = null;
    });

    Util.showSnackbarQuick(context, 'Getting current location');
  }

  Widget _generateArrival(BusServicesEstimate? svcEstimate) {
    if (svcEstimate == null) {
      // No services for this, return null data
      return _noArrivals();
    }

    return Expanded(
      child: IntrinsicWidth(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
              child: Center(
                child: Text(
                  (svcEstimate.minsLeft > 0)
                      ? '${svcEstimate.minsLeft}'
                      : 'Arr',
                  style: TextStyle(
                    color: (svcEstimate.minsLeft > 0)
                        ? (Util.themeNotifier.isDarkMode()
                            ? Colors.white
                            : Colors.black)
                        : Colors.green,
                  ),
                ),
              ),
            ),
            Spacer(),
            Divider(height: 0),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
              child: Center(
                child: Icon(
                  Icons.wheelchair_pickup,
                  size: 10,
                  color: (svcEstimate.wheelchairSupport)
                      ? (Util.themeNotifier.isDarkMode()
                          ? Colors.white
                          : Colors.black)
                      : Colors.grey,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Center(
                child: Text(
                  '${svcEstimate.busType}',
                  style: TextStyle(fontSize: 10),
                ),
              ),
            ),
            Spacer(),
            Divider(height: 0),
            Container(
              color: _getColorFromLoad(svcEstimate.loadLevel),
              height: 4,
            ),
          ],
        ),
      ),
    );
  }

  Widget _noArrivals() {
    return Expanded(
      child: IntrinsicWidth(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 4),
              child: Center(child: Text('-')),
            ),
            Spacer(),
            Divider(height: 0),
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 2),
              child: Center(
                child: Text('', style: TextStyle(fontSize: 10)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              child: Center(
                child: Text('', style: TextStyle(fontSize: 10)),
              ),
            ),
            Spacer(),
            Divider(height: 0),
            Container(height: 4),
          ],
        ),
      ),
    );
  }

  Color _getColorFromLoad(int load) {
    switch (load) {
      case 1:
        return Colors.green;
      case 2:
        return Colors.yellow;
      default:
        return Colors.red;
    }
  }

  Widget _nearbyStops(ScrollController sc) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            if (_supPc.isPanelOpen) {
              _supPc.close();
            } else {
              _supPc.open();
            }
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 16),
            child: Center(child: Text('Nearby Bus Stops')),
          ),
        ),
        Expanded(
          child: ListView.separated(
            controller: sc,
            itemCount: _nearbyBusStops?.length ?? 0,
            shrinkWrap: true,
            separatorBuilder: (context, i) {
              return Divider(height: 0);
            },
            itemBuilder: (BuildContext context, int i) {
              var key = _nearbyBusStops?.keys.toList()[i] ?? 0;
              var bData = _nearbyBusStops?[key];

              return Material(
                type: MaterialType.transparency,
                child: InkWell(
                  onTap: () => _selectCode(bData!),
                  child: Container(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('${bData?.description}'),
                            Text('${bData?.code}, ${bData?.road}'),
                            //Text('${bData?.code}, ${bData?.road}, Landmarks'),
                          ],
                        ),
                        Spacer(),
                        Text('${(key * 1000).toStringAsFixed(2)} m'),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  void _selectCode(BusStops key) {
    print(key);
    setState(() {
      _currentStop = key;
      _currentServices.clear();
    });
  }

  void _checkPermission() async {
    var permission = await Geolocator.checkPermission();
    setState(() {
      _hasLocationPerm = permission;
    });

    if (!_doIHaveLocationPermissions()) {
      _requestLocationPermissionPre();
    }
    _getCurrentLocation();
  }

  void _getServicesFromStop(BusStops stop) async {
    var stopCode = stop.code;
    var response = await http
        .get(Uri.parse('${Util.baseURL}/busarrival.php?BusStopID=$stopCode&SST=1'));
    print('Nearest Bus Stop HTTP Code: ${response.statusCode}');

    var data = jsonDecode(response.body);
    var time = DateTime.parse(data['CurrentTime']);

    var services = BusServices.fromDataset(data, curTime: time);
    setState(() {
      _currentServices = services;
    });
  }

  bool _doIHaveLocationPermissions() {
    switch (_hasLocationPerm) {
      case LocationPermission.denied:
        return false;
      case LocationPermission.deniedForever:
        return false;
      case LocationPermission.whileInUse:
        return true;
      case LocationPermission.always:
        return true;
      default:
        return false;
    }
  }

  void _noLocButtonClicked() async {
    if (_hasLocationPerm == LocationPermission.denied) {
      _requestLocationPermissionPre();
    } else {
      _openAppSettings();
    }
  }

  void _requestLocationPermissionPre() async {
    var showRationale = _hasLocationPerm == LocationPermission.denied;
    if (showRationale) {
      await showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Location Required'),
            content: SingleChildScrollView(
              child: ListBody(
                children: const <Widget>[
                  Text(
                    'In order for us to be able to get the nearest bus stops based on your location, we need access to your current location',
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () {
                  _requestLocationPermission();
                  Navigator.of(context).pop();
                },
                child: const Text('Grant Permission'),
              ),
            ],
          );
        },
      );
    } else {
      _requestLocationPermission();
    }
  }

  void _openAppSettings() async {
    print('Launching app settings');
    await Geolocator.openAppSettings();
  }

  void _requestLocationPermission() async {
    var permission = await Geolocator.requestPermission();
    setState(() {
      _hasLocationPerm = permission;
    });
    _getCurrentLocation();
  }

  void _getCurrentLocation() async {
    if (!_doIHaveLocationPermissions()) return;
    // Check service
    var status = await Geolocator.isLocationServiceEnabled();
    if (!status) {
      Util.showSnackbarQuick(
        context,
        'Location Service not enabled. Turn it on in your phone settings',
      );

      return;
    }

    // Get current location of user
    var position = await Geolocator.getLastKnownPosition(); // Get last known
    position ??= await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    print(
      'Position: ${position.latitude.toString()}, ${position.longitude.toString()}',
    );
    setState(() {
      _currentPosition = position;
    });

    _getNearbyStops();
  }

  void _getNearbyStops() async {
    var sig = await Util.getPackageSig();
    var pn = await Util.getPacakgeName();
    var response = await http.get(Uri.parse(
      '${Util.baseURL}/mobile/nearestBusStop.php?package=$pn&sig=$sig&location=${_currentPosition?.latitude},${_currentPosition?.longitude}',
    ));
    print('Nearest Bus Stop HTTP Code: ${response.statusCode}');
    //print(response.body);

    var jsonData = jsonDecode(response.body);
    var results = jsonData['results'];

    var stops = SortedMap<double, BusStops>(Ordering
        .byKey()); // Get new list of stops that are nearby (Distance, Stop)
    var box = await Hive.openBox<BusStops>('buses');

    for (var data in results) {
      var stopData = box.get(data['BusStopCode']);
      if (stopData == null) continue;
      stops[data['dist']] = stopData;
    }

    var nearestKey = stops.keys.toList()[0];
    var nearestData = stops[nearestKey];

    var selStop = nearestData;
    if (widget.parentCode != null) {
      var parStop = box.get(widget.parentCode);
      if (parStop != null) {
        selStop = parStop;
      }
    }

    await box.close();

    setState(() {
      _nearbyBusStops = stops;
      _currentStop = selStop;
    });
  }
}
