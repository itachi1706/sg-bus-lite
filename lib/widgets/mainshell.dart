import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sg_bus_lite/placeholder.dart';
import 'package:sg_bus_lite/widgets/busarrivals.dart';
import 'package:sg_bus_lite/widgets/favourites.dart';
import 'package:sg_bus_lite/widgets/search.dart';

class MainNavigationPage extends StatefulWidget {

  @override
  _MainNavigationPageState createState() => _MainNavigationPageState();
}

class _MainNavigationPageState extends State<MainNavigationPage> {

  List<Widget> _children = [];
  final List<String> _childTitle = [
    'Bus Arrivals',
    'Favourites',
    'Notifications',
    'Search',
  ];
  final List<Color> _childColors = [
    Colors.purple,
    Colors.orange,
    Colors.brown,
    Colors.indigo,
  ];
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();

    _children = [
      BusArrivalsTab(null),
      FavouritesTab(_busArrivalAtCode),
      PlaceholderWidgetContainer(Colors.green),
      SearchTab(_busArrivalAtCode),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_childTitle[_currentIndex]),
        actions: [
          IconButton(
            icon: Icon(Icons.map_sharp),
            tooltip: 'View bus stops location',
            onPressed: () => PlaceholderUtil.showUnimplementedSnackbar(context),
          ),
          IconButton(
            icon: Icon(Icons.settings),
            tooltip: 'Settings',
            onPressed: () => Get.toNamed('/settings'),
          ),
        ],
      ),
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: _childColors[_currentIndex],
          unselectedItemColor: Colors.grey,
          currentIndex: _currentIndex,
          onTap: _switchTabs,
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.directions_bus_sharp), label: 'Bus Arrivals'),
            BottomNavigationBarItem(icon: Icon(Icons.star), label: 'Favourites'),
            BottomNavigationBarItem(icon: Icon(Icons.bus_alert), label: 'Notifications'),
            BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Search'),
          ],
        ),
    );
  }
  
  void _busArrivalAtCode(String stopCode) {
    _children[0] = BusArrivalsTab(stopCode); // TODO: Pass code in
    print('Will head to $stopCode');
    setState(() {
      _currentIndex = 0;
    });
  }

  void _switchTabs(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}