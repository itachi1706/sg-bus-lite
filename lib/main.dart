// @dart=2.9

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sg_bus_lite/util.dart';
import 'package:sg_bus_lite/widgets/mainshell.dart';
import 'package:sg_bus_lite/widgets/settings.dart';
import 'package:sg_bus_lite/widgets/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  ThemeMode _theme = Util.themeNotifier.currentTheme();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Bus Where?',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Product-Sans',
      ),
      darkTheme: ThemeData(
        primarySwatch: Colors.indigo,
        fontFamily: 'Product-Sans',
        colorScheme: ColorScheme.dark().copyWith(primary: Colors.indigo),
      ),
      themeMode: _theme,
      initialRoute: '/splash',
      getPages: [
        GetPage(name: '/', page: () => MainNavigationPage()),
        GetPage(name: '/splash', page: () => SplashScreenPage()),
        GetPage(name: '/settings', page: () => SettingsPage()),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    Util.themeNotifier.toggleTheme();
    Util.themeNotifier.addListener(() {
      setState(() {
        _theme = Util.themeNotifier.currentTheme();
      });
    });
  }
}
