import 'package:flutter/material.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:package_signature/package_signature.dart';
import 'package:sg_bus_lite/listeners/themeNotifier.dart';
import 'package:url_launcher/url_launcher.dart';

class Util {
  static ThemeNotifier themeNotifier = ThemeNotifier();
  static final String baseURL = 'https://api.itachi1706.com/api';

  static void showSnackbarQuick(BuildContext context, String message) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message), duration: Duration(seconds: 2)),
    );
  }

  static Widget centerLoadingCircle(String loadText) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            SizedBox(height: 10),
            Text(loadText),
          ],
        ),
      );

  static Future<String> getPacakgeName() async {
    var packageInfo = await PackageInfo.fromPlatform();

    return packageInfo.packageName;
  }

  static Future<String> getPackageSig() async {
    if (GetPlatform.isAndroid) {
      var signature = await PackageSignature.signature;

      return signature.sha256;
    } else {
      return 'flutter';
    }
  }

  static Future<bool> launchWebPage(String url) async {
    // Web Browser for web mode
    if (GetPlatform.isAndroid || GetPlatform.isIOS) {
      // Native call for mobile app mode
      await FlutterWebBrowser.openWebPage(
        url: url,
        customTabsOptions: CustomTabsOptions(
          colorScheme: (Util.themeNotifier.isDarkMode())
              ? CustomTabsColorScheme.dark
              : CustomTabsColorScheme.light,
          addDefaultShareMenuItem: true,
          showTitle: true,
          urlBarHidingEnabled: true,
        ),
        safariVCOptions: SafariViewControllerOptions(
          barCollapsingEnabled: true,
          dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
          modalPresentationCapturesStatusBarAppearance: true,
        ),
      );

      return true;
    }

    // Launch web browser for all other platforms
    return await _launchWebPageWeb(url);
  }

  static Future<bool> _launchWebPageWeb(String url) async {
    // Launch through Web
    print('Launching $url');
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false);

      return true;
    } else {
      return false;
    }
  }
}
